<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


// Add Channels
$Q[] = "INSERT INTO `exp_channels`                                                                                                                                                        
(`channel_id`, `channel_name`, `channel_title`, `channel_url`, `channel_lang`, `total_entries`, `total_comments`, `last_entry_date`, `last_comment_date`, `cat_group`, `status_group`, `deft_status`, `field_group`, `search_excerpt`, `deft_category`, `deft_comments`, `channel_require_membership`, `channel_max_chars`, `channel_html_formatting`, `channel_allow_img_urls`, `channel_auto_link_urls`, `channel_notify`, `channel_notify_emails`, `comment_url`, `comment_system_enabled`, `comment_require_membership`, `comment_use_captcha`, `comment_moderate`, `comment_max_chars`, `comment_timelock`, `comment_require_email`, `comment_text_formatting`, `comment_html_formatting`, `comment_allow_img_urls`, `comment_auto_link_urls`, `comment_notify`, `comment_notify_authors`, `comment_notify_emails`, `comment_expiration`, `search_results_url`, `ping_return_url`, `show_button_cluster`, `rss_url`, `enable_versioning`, `max_revisions`, `default_entry_title`, `url_title_prefix`, `live_look_template`) VALUES 
('1', 'news', 'News', '".$this->userdata['site_url'].$this->userdata['site_index']."/news', 'en', 3, 0, '{$this->now}', 0, '1', 1, 'open', 1, 2, '', 'y', 'y', 0, 'all', 'y', 'y', 'n', '', '".$this->userdata['site_url'].$this->userdata['site_index']."/news/comments', 'y', 'n', 'y', 'n', 0, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '".$this->userdata['site_url'].$this->userdata['site_index']."/news/comments', '', 'y', '', 'n', 10, '', '', 0), 
('2', 'Site', 'Information Pages', '".$this->userdata['site_url'].$this->userdata['site_index']."/Site', 'en', 7, 0, '{$this->now}', 0, '2', 1, 'open', 2, 7, '', 'y', 'y', 0, 'all', 'y', 'n', 'n', '', '".$this->userdata['site_url'].$this->userdata['site_index']."/news/comments', 'y', 'n', 'y', 'n', 0, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '".$this->userdata['site_url'].$this->userdata['site_index']."/news/comments', '', 'y', '', 'n', 10, '', '', 0)";

// Add Field Groups
$Q[] = "INSERT INTO `exp_field_groups` (`group_id`, `site_id`, `group_name`) VALUES 
(1, 1, 'News'),
(2, 1, 'Site')";


// Add Custom Fields
$Q[] = "INSERT INTO `exp_channel_fields` (`field_id`, `site_id`, `group_id`, `field_name`, `field_label`, `field_instructions`, `field_type`, `field_list_items`, `field_pre_populate`, `field_pre_channel_id`, `field_pre_field_id`, `field_related_to`, `field_related_id`, `field_related_orderby`, `field_related_sort`, `field_related_max`, `field_ta_rows`, `field_maxl`, `field_required`, `field_text_direction`, `field_search`, `field_is_hidden`, `field_fmt`, `field_show_fmt`, `field_order`, `field_settings`, `field_content_type`) VALUES 
(1, 1, 1, 'news_body', 'Body', '', 'textarea', '', 'n', 0, 0, 'blog', 2, 'date', 'desc', 0, 10, 0, 'n', 'ltr', 'y', 'n', 'xhtml', 'y', 2, 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToieSI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJ5IjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToieSI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToieSI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6InkiO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6InkiO30=', 'any'),
(2, 1, 1, 'news_extended', 'Extended text', '', 'textarea', '', 'n', 0, 0, 'blog', 2, 'date', 'desc', 0, 12, 0, 'n', 'ltr', 'n', 'y', 'xhtml', 'y', 3, 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToieSI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJ5IjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToieSI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToieSI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6InkiO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6InkiO30=', 'any'),
(3, 1, 1, 'news_image', 'News Image', '', 'file', '', 'n', 0, 0, 'blog', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=', 'image'),
(4, 1, 2, 'site_body', 'Body', '', 'textarea', '', 'n', 0, 0, 'blog', 3, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'xhtml', 'y', 4, 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToieSI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJ5IjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToieSI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToieSI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6InkiO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6InkiO30=', 'any'),
(5, 1, 2, 'site_image', 'Image', 'URL Only', 'file', '', 'n', 0, 0, 'blog', 3, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=', 'image'),
(6, 1, 2, 'site_optional', 'Staff Member''s Title', 'This is the Title that the staff member has within the company.  Example: CEO', 'text', '', 'n', 0, 0, 'blog', 3, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 6, 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=', 'any'),
(7, 1, 2, 'site_extended', 'Extended', '', 'textarea', '', 'n', 0, 0, 'blog', 3, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'y', 'xhtml', 'y', 7, 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToieSI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJ5IjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToieSI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToieSI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6InkiO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6InkiO30=', 'any')";

// Add columns to data and formatting tables
foreach (array(1,2,3,4,5,6,7) as $id)
{
	$Q[] = "ALTER TABLE `exp_channel_data` ADD COLUMN `field_id_{$id}` text NULL";
	$Q[] = "ALTER TABLE `exp_channel_data` ADD COLUMN `field_ft_{$id}` tinytext NULL";
	$Q[] = "INSERT INTO exp_field_formatting (field_id, field_fmt) VALUES ({$id}, 'none')";
	$Q[] = "INSERT INTO exp_field_formatting (field_id, field_fmt) VALUES ({$id}, 'br')";
	$Q[] = "INSERT INTO exp_field_formatting (field_id, field_fmt) VALUES ({$id}, 'xhtml')";
}

// Create default categories
$Q[] = "INSERT INTO `exp_category_groups` (`group_id`, `site_id`, `group_name`, `sort_order`, `field_html_formatting`, `can_edit_categories`, `can_delete_categories`) VALUES 
(1, 1, 'News Categories', 'a', 'all', '', ''),
(2, 1, 'Site', 'a', 'all', '', '')";


$Q[] = "INSERT INTO `exp_categories` (`cat_id`, `site_id`, `group_id`, `parent_id`, `cat_name`, `cat_url_title`, `cat_description`, `cat_image`, `cat_order`) VALUES 
(1, 1, 1, 0, 'News', 'News', '', '', 2),
(2, 1, 1, 0, 'Products', 'Products', '', '', 3)";


$Q[] = "INSERT INTO `exp_category_field_data` (`cat_id`, `site_id`, `group_id`) VALUES 
(1, 1, 1),
(2, 1, 1),
(3, 1, 2),
(4, 1, 2)";

// Add Agile Specific Custom Status
$Q[] = "INSERT INTO `exp_statuses` (`status_id`, `site_id`, `group_id`, `status`, `status_order`, `highlight`) VALUES
        (3, 1, 1, 'Featured', 3, '000000')";

// Add default entries
$Q[] = "INSERT INTO `exp_channel_titles` (`entry_id`, `channel_id`, `author_id`, `ip_address`, `title`, `url_title`, `status`, `entry_date`, `year`, `month`, `day`, `edit_date`) VALUES 
(1, 1, 1, '".$this->input->ip_address()."', 'Getting to Know ExpressionEngine', 'getting_to_know_expressionengine', 'open', '".($this->now - 1)."', '".$this->year."', '".$this->month."', '".$this->day."', '".date("YmdHis")."'),
(2, 1, 1, '".$this->input->ip_address()."', 'Welcome to the Example Site!', 'welcome_to_the_example_site', 'open', '".$this->now."', '".$this->year."', '".$this->month."', '".$this->day."', '".date("YmdHis")."')";

$Q[] = "INSERT INTO `exp_category_posts` (`entry_id`, `cat_id`) VALUES 
(1, 1),
(2, 1),
(3, 4),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 2)";

// Add upload locations
$Q[] = "INSERT INTO `exp_upload_prefs` (`id`, `site_id`, `name`, `server_path`, `url`, `allowed_types`, `max_size`, `max_height`, `max_width`, `properties`, `pre_format`, `post_format`, `file_properties`, `file_pre_format`, `file_post_format`) VALUES 
(1, 1, 'Main Upload Directory', '".$this->userdata['image_path'].$this->userdata['upload_folder']."', '".$this->userdata['site_url'].'images/'.$this->userdata['upload_folder']."', 'all', '', '', '', 'style=\"border: 0;\" alt=\"image\"', '', '', '', '', ''),
(2, 1, 'Site', '".$this->theme_path."/images/uploads/', '".$this->userdata['site_url']."/images/uploads/', 'img', '', '', '', '', '', '', '', '', '')";

chmod($this->theme_path."/images/uploads/", DIR_WRITE_MODE);

foreach ($Q as $sql)
{
	$this->db->query($sql);
}

// Set the Member profile theme default, and Strict 404 settings
$this->config->update_site_prefs(array(
										'member_theme'	=> '',
										'strict_urls'	=> 'y',
										'site_404'		=> '404'
										),
										1 // site id
								);

/* End of file default_content.php */
/* Location: ./themes/site_themes/tdc/default_content.php */