<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Pages AutoSave URI Extension class
*
* @package			-
* @version			1.0
* @author			Johan Strömqvist <johan@naboovalley.com>
* @link				http://naboovalley.com
* @license			http://creativecommons.org/licenses/by-sa/3.0/
*/
class Pages_autosave_uri_ext
{
	var $settings 		= array();
	var $name 			= 'Pages AutoSave URI';
	var $version 		= '1.0';
	var $description 	= 'Automatically saves your url_title field to your pages URI';
	var $settings_exist = 'n';
	var $docs_url 		= '';
    
    var $format = TRUE;
    
	/**
	* Class constructor
	*
	* @return	null
	*/
    function Pages_autosave_uri_ext($settings='')
    {
        $this->settings = $settings;
		
        $this->EE =& get_instance();
    }

	// --------------------------------------------------------------------
	
	/**
	* Fires when an entry is added and/or updated
	*
	* @return	null
	*/
	function on_entry_change($entry_id, $meta, $data)
	{
		// Variables
		$site_pages_array 	= $this->get_sites();
		$site_id 			= $this->EE->config->item('site_id');
		$template_id		= $data["revision_post"]["pages__pages_template_id"];
		$saved_pages_uri	= $data["revision_post"]["pages__pages_uri"];
		$url_title			= $data["revision_post"]["url_title"];
		
		// Check if page is set or not
		if(isset($site_pages_array[$site_id]['uris'][$entry_id]))
		{
			// Page is set - Update old page URI with new if it's changed
			if($saved_pages_uri != "/".$url_title)
			{
				$site_pages_array[$site_id]['uris'][$entry_id] 		= $url_title;
				$site_pages_array[$site_id]['templates'][$entry_id] = $template_id;				
			
				$this->set_sites($site_pages_array);
			}
			
		} else {
		
			// Page URI isn't set. Set it!
			$site_pages_array[$site_id]['uris'][$entry_id] 		= "/".$url_title;	
			$site_pages_array[$site_id]['templates'][$entry_id] = $template_id;
			
			$this->set_sites($site_pages_array);
		}
	}
	
	// --------------------------------------------------------------------
	
	private function get_sites()
	{
					$this->EE->db->select('site_pages')->where('site_id', $this->EE->config->item('site_id'));
		$query = 	$this->EE->db->get('sites');

		return unserialize(base64_decode($query->row('site_pages')));
	}
	
	private function set_sites($updated_site_pages_array)
	{
		$array = base64_encode(serialize($updated_site_pages_array));
		
		$data = array(
			'site_pages ' => $array
        );
		
		$this->EE->db->where('site_id', $this->EE->config->item('site_id'));
		$this->EE->db->update('sites', $data);
	}

	// --------------------------------------------------------------------
	
	/**
	* Activate extension
	*
	* @return	null
	*/
	function activate_extension()
	{
		// data to insert
		$data = array(
			'class'		=> __CLASS__,
			'method'	=> 'on_entry_change',
			'hook'		=> 'entry_submission_end',
			'priority'	=> 1,
			'version'	=> $this->version,
			'enabled'	=> 'y',
			'settings'	=> ''
		);
		
		// insert in database
		$this->EE->db->insert('exp_extensions', $data);
	}
	 
	// --------------------------------------------------------------------
	
	/**
	* Update extension
	*
	* @param	string	$current
	* @return	null
	*/
	function update_extension($current = '')
	{
	}

	// --------------------------------------------------------------------
	
	/**
	* Disable extension
	*
	* @return	null
	*/
	function disable_extension()
	{
		// Delete records
		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->delete('exp_extensions');
	}

	// --------------------------------------------------------------------
	 
}
// END CLASS

/* End of file */