<?php 	

/*
=====================================================
 Author: MaxLazar
 http://www.wiseupstudio.com
=====================================================
 File: pi.mobile_detect.php
-----------------------------------------------------
 Purpose: Detect Mobile Device Requests, ExpressionEngine2
=====================================================
*/




$plugin_info = array(
						'pi_name'			=> 'MX Mobile Detect',
						'pi_version'			=> '2.1',
						'pi_author'			=> 'Max Lazar',
						'pi_author_url'		=> 'http://wiseupstudio.com/',
						'pi_description'	=> 'Detect Mobile Device Requests',
						'pi_usage'			=> mobile_detect::usage()
					);


class Mobile_detect {

    var $return_data="";
    
    function Mobile_detect ()
    {    
	
		$this->EE =& get_instance();


		$client = new Client();//Redirection Method 
        $tagdata = $this->EE->TMPL->tagdata;
		$conds['mobile'] = $client->isMobileClient($_SERVER['HTTP_USER_AGENT']);
		
		if ($tagdata == '') {
			$location = ( ! $this->EE->TMPL->fetch_param('location')) ?  ('') : $this->EE->TMPL->fetch_param('location');
			
			if  ($conds['mobile'] === true){
				return $this->return_data =  '<script type="text/javascript">location.href="'.$location.'";</script>' ;
				}
		}	
		else
		{
		 $tagdata = $this->EE->functions->prep_conditionals($tagdata, $conds); 
		 $tagdata = $this->EE->TMPL->swap_var_single("mobile", $conds['mobile'], $tagdata); 
		 return $this->return_data = $tagdata;
		}
       
    }
    
    
// ----------------------------------------
//  Plugin Usage
// ----------------------------------------

// This function describes how the plugin is used.
//  Make sure and use output buffering

function usage()
{	
ob_start(); 
?>
Place the following tag in your templates (it would be better if your place it close to the header);

{exp:mobile_detect location="http://m.site.com/"}

"location" - is your url for mobile devices

Or you can use that way:

{exp:mobile_detect}
{if mobile}
Is mobile device
{/if}
{/exp:mobile_detect}


<?php
$buffer = ob_get_contents();
	
ob_end_clean(); 

return $buffer;
}
/* END */

}


class Client
{
	/**
	 * Available Mobile Clients
	 *  http://www.zytrax.com/tech/web/mobile_ids.html
	 * @var array
	 */
	private $_mobileClients = array(
		"midp",
		"240x320",
		"blackberry",
		"netfront",
		"nokia",
		"panasonic",
		"portalmmm",
		"sharp",
		"sie-",
		"sonyericsson",
		"symbian",
		"windows ce",
		"benq",
		"mda",
		"mot-",
		"opera mini",
		"philips",
		"pocket pc",
		"sagem",
		"samsung",
		"sda",
		"sgh-",
		"vodafone",
		"xda",
		"iphone",
		"android"
	);

	/**
	 * Check if client is a mobile client
	 * @param string $userAgent
	 * @return boolean
	 */
	public function isMobileClient($userAgent)
	{
		$userAgent = strtolower($userAgent);
		foreach($this->_mobileClients as $mobileClient) {
			if (strstr($userAgent, $mobileClient)) {
				return true;
			}
		}
		return false;
	}
	


}


?>